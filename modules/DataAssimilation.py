import numpy as np
import pandas as pd
from numpy.linalg import inv

class DataAssimilation:
    def __init__(self,mrl,obsl):
        self.mrl = mrl
        self.obsl = obsl

    def setup_H(self):
        #ustawiamy macierz obserwacji tak, aby obserwowała wybrane stacje
        dimX = self.mrl.get_spatial_shape()[0]
        dimY = self.mrl.get_spatial_shape()[1]
        assim_inds = self.obsl.get_assim_stations_idx()
        H=np.eye(1,dimX*dimY,k=3)

        for i in range(len(assim_inds)):
            k1= dimY*assim_inds[i][1] +assim_inds[i][0]
            H=np.concatenate([H ,np.eye(1,dimX*dimY,k=k1)])
        #usuwanie zerowego wiersza
        H=np.delete(H, obj=0, axis=0)
        self.H = H
    
    def calculate_station_distance(self,i,j):
        #funkcja liczy odległość euklidesową między stacją i-tą a j-tą w km
        try:
            st1 = self.obsl.get_assim_stations_names()[i]
            st2 = self.obsl.get_assim_stations_names()[j]
            d0=self.obsl.get_station_coordinates92(st1)[0]-self.obsl.get_station_coordinates92(st2)[0]
            d1=self.obsl.get_station_coordinates92(st1)[1]-self.obsl.get_station_coordinates92(st2)[1]
            return np.sqrt((d0**2+d1**2))/1000
        except Exception as e:
            print("Unable to calculate distance "+str(i)+" "+str(j))
            print(self.obsl.get_assim_stations_names())
            raise e

    def calculate_station_grid_distance(self,j,i,k):
        return np.sqrt((i - self.obsl.st_assim_indNC[k][0])**2+(j - self.obsl.st_assim_indNC[k][1])**2)*2.7

    def setup_uncertainty(self,NO,sigma_b,sigma_o,corr_length):
        # NO - liczba obserwacji
        # tzw. ręcznie ustawianie niepewności
        self.sigma_o=sigma_o
        self.sigma_b=sigma_b
        self.corr_length=corr_length
        R = np.zeros([NO,NO])
        HPfHT=np.zeros([NO,NO])
        for i in range(NO):
            for j in range(NO):
                if(i == j):
                    R[i,j]=self.sigma_o
                
                dist = self.calculate_station_distance(i,j)
                HPfHT[i,j] = self.sigma_b*np.exp(-(dist/self.corr_length))
        
        self.A = HPfHT+R
        self.R = R

    def setup_uncertainty2(self,NO,obs_var,corr_length):
        # NO - liczba obserwacji
        # wersja ze zróżnicowana niepewnością (Zhang et. al. 2018)
        self.sigma_o=np.mean(obs_var)
        self.sigma_b=113
        self.corr_length=corr_length

        R = np.zeros([NO,NO])
        HPfHT=np.zeros([NO,NO])
        for i in range(NO):
            for j in range(NO):
                if(i == j):
                    R[i,j]=obs_var.iloc[i]
                
                dist = self.calculate_station_distance(i,j)
                HPfHT[i,j] = self.sigma_b*np.exp(-(dist/self.corr_length))
        
        self.A = HPfHT+R
        self.R = R

    def setup_HPfT(self,NO):
        (ni,nj) = self.mrl.get_grid_dimensions()
        sigma_b_ij = 1
        self.HPfT=np.zeros([ni*nj,NO])

        for i in range(ni):
            for j in range(nj):
                for k in range(NO):
                    dist = self.calculate_station_grid_distance(j,i,k)
                    wsp = ni*j+i
                    self.HPfT[wsp,k]=sigma_b_ij*self.sigma_b*np.exp(-(dist/self.corr_length))
                    

    def setup_K(self):
        self.K = self.HPfT @ inv(self.A)

    def perform_fusion(self,xb,xo):
        y = self.H @ xb
        #in case of empty observations
        xo[np.where(np.isnan(xo))]=y[np.where(np.isnan(xo))]
        #calculating innovation
        Innov = (xo.reshape(y.shape) - y)
        #performing assimilation
        xa = xb + (self.K @ (Innov))
        return xa


    def plot_K(self):
        sns.heatmap(self.K)
        plt.show()

    def plot_R(self):
        sns.heatmap(self.R)
        plt.show()

    def plot_HPfT(self):
        sns.heatmap(self.HPfT)
        plt.show()