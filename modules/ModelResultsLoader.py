
import numpy as np
from netCDF4 import Dataset

class ModelResultsLoader:
    folder = 'diag_conc_nc/'
    def __init__(self,species1):
        self.species=species1

    def load_model(self,data):
        file1=Dataset(self.folder+data+"/"+data+"_gemaq_file_4.nc",format="NETCDF3_CLASSIC")
        vars1 =file1.variables
        self.model_array = np.squeeze(vars1[self.species])        
        self.lon = np.array(vars1['lon'])
        self.lat = np.array(vars1['lat'])

    def find_nearest_lon(self, lon_value):
        idx = (np.abs(self.lon - lon_value)).argmin()
        return idx
    
    def find_nearest_lat(self, lat_value):
        idx = (np.abs(self.lat - lat_value)).argmin()
        return idx

    def get_grid_dimensions(self):
        return (len(self.lon),len(self.lat))
        
    def get_coordinates_WGS(self,i,j):
        return (self.lon[i],self.lat[j])

    def get_temporal_shape(self):
        return self.model_array.shape[0]

    def get_spatial_shape(self):
        return self.model_array.shape[1:]
    
    def get_lats(self):
        return self.lat
    
    def get_lons(self):
        return self.lon

    def get_array(self,hour):
        if hour<self.model_array.shape[0]:
            return self.model_array[hour]
        else:
            raise IndexError