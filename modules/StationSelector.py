import pandas as pd
import random
import math

#narzędzia do wyboru asymilowanych stacji

class StationSelector:
    def __init__(self,obsl):
        self.obsl = obsl
        self.N = len(self.obsl.df.columns)
        self.names = set(self.obsl.df.columns)
        print("Załadowano "+str(self.N)+" stacji pomiarowych")

    def select_N_random_sample(self,n):
        self.selected = random.sample(self.names,n)
        print("Wybrano losowo "+str(n)+" stacji")
        return self.selected

    def set_selected_stations(self,list_selected):
        #in case of external selection
        self.selected = list_selected
        
    def get_selected_variance(self):
        if len(self.selected)>0:
            return self.obsl.df[self.selected].var()

    def select_perc_random_sample(self,perc):
        return self.select_N_random_sample(math.floor(self.N*perc/100))

    def select_validation_stations(self,n=0):
        # if 0 than all avaliable
        pozostale = set(self.names) - set(self.selected)

        if n>0 and n<=len(pozostale):
            return random.sample(pozostale,n)
        else:
            return pozostale

    def get_selected(self):
        return self.selected