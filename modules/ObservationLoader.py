import pandas as pd

class ObservationLoader:
    folder = 'csv_obs/'
    def __init__(self,species1):
        self.df = pd.read_csv(self.folder+"ts_"+species1+"_2019.csv")
        self.df.set_index(['Unnamed: 0'],inplace=True)
        self.all_stations_names = self.df.columns

        self.df_locations = pd.read_csv(self.folder+"all_stations92.csv")
        self.df_locations.set_index(['station_code'],inplace=True) 

    def get_head(self):
        if not self.df.empty:
            return self.df.head()
    
    def get_variance(self):
        if not self.df.empty:
            return self.df.var()
    
    def get_assim_stations_idx(self):
        return self.st_assim_indNC
    
    def get_assim_stations_names(self):
        return self.st_assim_names

    def get_valid_stations_idx(self):
        return self.st_valid_indNC
    
    def get_valid_stations_names(self):
        return self.st_valid_names

    def get_number_valid_stations(self):
        return len(self.st_valid_names)

    def get_number_assim_stations(self):
        return len(self.st_assim_names)

        
    def get_all_obs_stations_names(self):
        return self.df.columns

    def get_station_coordinates92(self,nazwa):
        #masakrycznie wolna funkcja, unikać jak ognia!
        return (self.df_locations.loc[nazwa]['station_x'],self.df_locations.loc[nazwa]['station_y'])
    
    def get_station_coordinatesWGS(self,nazwa):
        #masakrycznie wolna funkcja, unikać jak ognia!
        return (self.df_locations.loc[nazwa]['station_long'],self.df_locations.loc[nazwa]['station_lat'])
    
    def get_istation_coordinatesWGS(self,id):
        #szybki wybór z tablicy podręcznej
        return self.st_assim_WGS_cors[id]
    
    def get_istation_coordinates92(self,id):
        #szybki wybór z tablicy podręcznej
        return self.st_assim_92_cors[id]

    def select_validation_stations(self,stations,mrl):

        self.st_valid_names = []
        self.st_valid_indNC = []
        self.st_valid_WGS_cors = []
        self.st_valid_92_cors = []
        for nazwa in stations:
            if(nazwa in self.all_stations_names):
                
                lat = self.df_locations.loc[nazwa]['station_lat']
                lon = self.df_locations.loc[nazwa]['station_long']
                X = self.df_locations.loc[nazwa]['station_x']
                Y = self.df_locations.loc[nazwa]['station_y']
                #ideksy statcji na siatce modelu
                idx_lat = mrl.find_nearest_lat(lat)
                idx_lon = mrl.find_nearest_lon(lon)
                
                self.st_valid_indNC.append((idx_lon,idx_lat))
                self.st_valid_names.append(nazwa)
                self.st_valid_WGS_cors.append( (lon,lat) )
                self.st_valid_92_cors.append( (X,Y) )
            else:
                print("Nie znaleziono stacji do walidacji "+nazwa)

    def select_assim_stations(self,stations,mrl):
        
        self.st_assim_names = []
        self.st_assim_indNC = []
        self.st_assim_WGS_cors = []
        self.st_assim_92_cors = []

        for nazwa in stations:
            if(nazwa in self.all_stations_names):
                
                lat = self.df_locations.loc[nazwa]['station_lat']
                lon = self.df_locations.loc[nazwa]['station_long']
                X = self.df_locations.loc[nazwa]['station_x']
                Y = self.df_locations.loc[nazwa]['station_y']
                #ideksy statcji na siatce modelu
                idx_lat = mrl.find_nearest_lat(lat)
                idx_lon = mrl.find_nearest_lon(lon)
                
                self.st_assim_indNC.append((idx_lon,idx_lat))
                self.st_assim_names.append(nazwa)
                self.st_assim_WGS_cors.append( (lon,lat) )
                self.st_assim_92_cors.append( (X,Y) )
            else:
                print("Nie znaleziono stacji "+nazwa)
        
    def get_observations(self):
        return self.df[self.st_assim_names].to_numpy()

    def get_validations(self):
        return self.df[self.st_valid_names].to_numpy()
