#local modules
from modules.ModelResultsLoader import ModelResultsLoader
from modules.ObservationLoader import ObservationLoader
from modules.DataAssimilation import DataAssimilation
from modules.OutputProcessor import OutputProcessor
from modules.StationSelector import StationSelector

import numpy as np
import pandas as pd
from numpy.linalg import inv

#
from netCDF4 import Dataset
#from osgeo import osr 
#from osgeo import gdal
#różne pomocnicze
import time
import pprint
import datetime
# dla plotowania wyników
import seaborn as sns
import matplotlib.pyplot as plt
from pylab import rcParams

#długość obliczeń
days = 105
#sposób spłaszczania macierzy
rs='A'
#liczba stacji asymilowanych i walidowanych (o ile nie obliczono inaczej)
NO = 13
NV = 7
param = 'NO2'


def main():
    start = time.time()
    d0 =datetime.datetime(2019,1,1)
    mrl = ModelResultsLoader(param)
    obsl = ObservationLoader(param)
    ss = StationSelector(obsl)

    mrl.load_model('20190101')
    

    #stacje do asymilacji
    tymczas=["DsDzialoszyn","WpPoznDabrow","OpOpoleOsAKr","SlRybniBorki","MpKrakBulwar","PkKrempnaMPN","MzBelsIGFPAN","LdZgieMielcz", \
        "LbZamoHrubie","WmElkStadion","PmLebMalcz16","ZpSzczAndr01","LuZielKrotka"]
    ss.set_selected_stations(tymczas)
    #to nei działa -> wyjaśnić dlaczego
    #tymczas = obsl.get_all_obs_stations_names()
    #tymczas = ss.select_N_random_sample(NO)
    obs_variance = ss.get_selected_variance()

    walidki = ss.select_validation_stations(NV)
    obsl.select_assim_stations(tymczas,mrl)
    obsl.select_validation_stations(walidki,mrl)
    op = OutputProcessor(param,obsl)
    op.prepare_stat_output(filename='res_stats',lons=mrl.get_lons(),lats=mrl.get_lats())
    op.init_stats(param)

    da = DataAssimilation(mrl,obsl)
    da.setup_H()
    # ręczne zadawanie niepewności
    #da.setup_uncertainty(NO, sigma_b=113, sigma_o=91, corr_length=20)
    da.setup_uncertainty2(NO,obs_var=obs_variance,corr_length=20)
    da.setup_HPfT(NO)
    da.setup_K()
    Xo = obsl.get_observations()
    Xv = obsl.get_validations()
    step = 0
    print('Execution time %4.4f s' % (time.time() - start))   
    print("Setup finished. Starting assimilation iterations.")
    for day in range(days):
        data_dt = d0+ datetime.timedelta(days=day)
        data =(data_dt).strftime('%Y%m%d')

        print(data)
        mrl.load_model(data)
        dimX = mrl.get_spatial_shape()[0]
        dimY = mrl.get_spatial_shape()[1]
        total_size = dimX*dimY
        op.setup_output(data,dimY,dimX)
        op.setup_dims(mrl.get_lons(),mrl.get_lats())
        for hour in range(24):
            xb = mrl.get_array(hour).flatten(rs).reshape(dimX*dimY,1)
            xo = np.array(Xo[step]).reshape(NO,1)
            xv = np.array(Xv[step])

            xb[np.where(np.isnan(xb))]=0
            xa = da.perform_fusion(xb,xo)

            op.save_ts(step,hour,xa.reshape((300,470),order=rs),xv)
            op.update_stats(dzien=day,godzina=hour,da=xa.reshape((300,470)))

            step = step +1

        op.flush_output()
    op.flush_stat_output()
    
    #da.plot_K()
    print('Total execution time %4.4f s' % (time.time() - start))    


def main2():
    obsl = ObservationLoader('NO2')
    ss = StationSelector(obsl)
    ss.select_N_random_sample(130)
    walidki = ss.select_validation_stations(3)

if __name__=='__main__':
    main()  