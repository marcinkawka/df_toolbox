import numpy as np
import pandas as pd
from netCDF4 import Dataset
import datetime



class OutputProcessor:
    def __init__(self,species,obsl):
        self.obsl = obsl
        self.param = species
        self.folder = 'results_nc/'
        self.bias = np.empty(obsl.get_number_valid_stations())
        self.obs = np.empty(obsl.get_number_valid_stations())

    def setup_output(self,data,lon_size,lat_size):
        self.output = Dataset(self.folder+'res_'+data+'.nc','w',format='NETCDF3_CLASSIC')
        self.output.createDimension('lon',lon_size)
        self.output.createDimension('lat',lat_size)
        self.output.createDimension('time',None)

    def setup_dims(self,lons,lats,time_horizon=24):        
        self.output.createVariable('time', 'float32', ('time',))
        self.output['time'][:]=np.arange(time_horizon)
        self.output.createVariable('lon', 'float32', ('lon',))
        self.output['lon'][:] = lons
        self.output.createVariable('lat','float32', ('lat',))
        self.output['lat'][:] = lats
        self.output.createVariable(self.param, 'float32',('time',  'lat', 'lon'))

    def save_ts(self,step,hour,xa,xv):
        self.output[self.param][hour] = xa
        self.calculate_validation(step,hour,xa,xv)

    def calculate_validation(self,step,hour,xa,xv):
        valid_ind_NC = self.obsl.get_valid_stations_idx()
        k=0
        for j,i in valid_ind_NC:
            if(~np.isnan(xv[k])):
                self.bias[k] = self.bias[k] + (xv[k] - xa[i][j] )
                self.obs[k] = self.obs[k] + xv[k]
            k = k+1
        

    def flush_output(self):    
        self.output.close()
        self.output = None

    def prepare_stat_output(self,filename,lons,lats):
        #przygotowywanie pliku wynikowego
        self.st_output = Dataset('results_nc/'+filename+'.nc','w',format='NETCDF3_CLASSIC')

        self.st_output.createDimension('lon', 470)
        self.st_output.createDimension('lat', 300)

        self.st_output.createVariable('lon', 'float32', ('lon',))
        self.st_output['lon'][:] = lons
        self.st_output.createVariable('lat','float32', ('lat',))
        self.st_output['lat'][:] = lats
        

    def init_stats(self,param,dt=24):
        self.da_min24=999.9*np.ones([300,470])
        self.da_max24=-999.9*np.ones([300,470])
        self.da_sum24=np.zeros([300,470])
        self.d0 =datetime.datetime(2019,1,1)
        self.param = param
        self.steps =0
    
    def update_stats(self,dzien,godzina,da):
        data =(self.d0+ datetime.timedelta(days=dzien)).strftime('%Y%m%d')
        self.da_min24 = np.min([self.da_min24,da],axis=0)
        self.da_max24 = np.max([self.da_max24,da],axis=0)
        self.da_sum24 = np.sum([self.da_sum24,da],axis=0)
        self.steps = self.steps +1
        

    def flush_stat_output(self):
        self.da_avg24 = self.da_sum24/(self.steps)
        self.st_output.createVariable(self.param+'_MIN','float32', ('lat','lon'))
        self.st_output.createVariable(self.param+'_MAX','float32', ('lat','lon'))
        self.st_output.createVariable(self.param+'_AVG','float32', ('lat','lon'))
        self.st_output[self.param+'_MIN'][:] = self.da_min24
        self.st_output[self.param+'_MAX'][:] = self.da_max24
        self.st_output[self.param+'_AVG'][:] = self.da_avg24
        self.st_output.close()
        self.st_output = None

        np.set_printoptions(precision=1)
        print("\n Mean Bias per station [ug/m3]:")
        print(self.obsl.get_valid_stations_names())
        print(self.bias/self.steps)
        
        print("\n Relative Bias per station [%]:")
        print(self.obsl.get_valid_stations_names())
        print(self.bias/self.obs*100)